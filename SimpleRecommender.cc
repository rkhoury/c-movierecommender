// Rayyan Khoury
// 260257960
// COMP 322
// Assignment 3

#include "SimpleRecommender.h"

double SimpleRecommender::generatePrediction(int userid, string movieName) {

	// sets a local variable 
	//vector<MovieInformation> vec;
	//vec = movies;
	
	// sets our variables
	int counter = 0;
	double total = 0.0;
	double average = 0.0;
	int id = 0;
	bool found = false;
	
	// iterates through the vector of movie informations and if it finds the movie, breaks
	for (vector<MovieInformation>::iterator it = SimpleRecommender::movies.begin(); it != SimpleRecommender::movies.end(); ++it) {
	
		if ((*it).getName().compare(movieName) == 0) {
		
			id = (*it).getId();
			found = true;
			break;
			
		}
		
	}
	
	// if nothing was found, return 0.0
	if (!found) {
	
		return 0.0;
		
	}
	
	// sets a local variable 
	//list<RatingInformation> rat = ratings;
	
	// goes through the list of rating information
	for (list<RatingInformation>::iterator it = SimpleRecommender::ratings.begin(); it != SimpleRecommender::ratings.end(); ++it) {
	
		if ((*it).movieId == id) {
		
			// If the movie has already been rated by the user it returns 0 as it does not want to recommend this movie
			if ((*it).movieId == userid) {
			
				return 0.0;
				
			}
			
			// adds the score to the total and upps the counter
			total += (*it).rating;
			counter++;
			
		}
		
	}
	
	// solves the average
	if (counter != 0) {
	
		average = total/counter;
	
	}
	
	return average;

}
        
vector<string> SimpleRecommender::makeRecommendations(int userid, int n) {

	vector<string> a;
	int counter = 0;

	// returns empty vector
	if (n == 0) {
	
		return a;
		
	}

	// sets some variables used in the following for loops
	vector<double> scores;
	vector<string> names;
	int scoresSize = 0;
	int namesSize = 0;
	
	// sets a local variable 
	//vector<MovieInformation> vec = movies;
	
	// sets the scores and the names of each movie 
	for (vector<MovieInformation>::iterator it = SimpleRecommender::movies.begin(); it != SimpleRecommender::movies.end(); ++it) {
	
		names.push_back((*it).getName());
		scores.push_back(generatePrediction(userid, (*it).getName()));
		
	}
	
	// stores the size of the two vectors
	namesSize = names.size();
	scoresSize = scores.size();
	
	// if the vectors are unequal then the function has failed
	if (scoresSize != namesSize) {
	
		return a;
	
	}
	
	// sets a double max as the max value and a maxlessb as the minimum value
	double max = *max_element(scores.begin(),scores.end()) + 1.0;
	double maxLess = *min_element(scores.begin(),scores.end()) - 1.0;
	
	vector<double> scoresSorted;
	vector<string> namesSorted;
	counter = 0;
	
	// sorts both vectors simultaneously
	for (int i = 0; i < scoresSize; i++) {
	
		maxLess = *min_element(scores.begin(),scores.end()) - 1.0;
	
		for (int j = 0; j < scoresSize - i; j++) {
		
			if (maxLess <= scores[j] && scores[j] <= max) {
			
				maxLess = scores[j];
				counter = j;
			
			}
			
		}
		
		// adds the highest scoring movie and score
		scoresSorted.push_back(scores[counter]);
		namesSorted.push_back(names[counter]);
		
		// erases from initial vector
		scores.erase(scores.begin()+counter);
		names.erase(names.begin()+counter);
		
		// sets the max to the max found here as a constraint
		max = maxLess;
	
	}
	
	// trims the vector of zeroes
	for (int i = 0; i < scoresSize; i++) {
	
		if (scoresSorted[i] == 0.0) {
		
			scoresSorted.erase(scoresSorted.begin()+i, scoresSorted.end());
			namesSorted.erase(namesSorted.begin()+i, namesSorted.end());
			break;
		
		}
	
	}
	
	int sizeScoresSorted = scoresSorted.size();
	int sizeNamesSorted = namesSorted.size();
	
	// fails at this point if the size of the vectors are unequal
	if (sizeScoresSorted != sizeNamesSorted) {
	
		return a;
	
	}
	
	// makes sure that the number of movies returned is correct by cutting the vector
	if (n < sizeNamesSorted) {
	
		namesSorted.erase(namesSorted.begin()+n, namesSorted.end());
	
	}
	
	// returns the vector
	return namesSorted;

}

// loads the movie list from the file
void SimpleRecommender::loadMovieListFromFile(string filename){
        
	ifstream in_file;
	
	char *a = new char[filename.size()+1];
	a[filename.size()]=0;
	memcpy(a,filename.c_str(),filename.size());
	
	in_file.open(a);
	
	if(!in_file) {
        cout << "movie file failed" << endl;
		return;
    } 
	
	string lineTemp;
	int counter = 0;
	
	while(getline(in_file, lineTemp)) {
	
		MovieInformation movieTemp = MovieInformation::MovieInformation(lineTemp, counter);
		SimpleRecommender::movies.push_back(movieTemp);
		
		counter++;
		
	}
	
	in_file.close();
	return;
	
}

// loads the ratings list from the file as used in the A2 solutions
void SimpleRecommender::loadRatingsFromFile(string filename){

	ifstream in_file;
	
	char *a = new char[filename.size()+1];
	a[filename.size()]=0;
	memcpy(a,filename.c_str(),filename.size());
	
    in_file.open(a);
	
	if(!in_file) {
        cout << "rating file failed" << endl;
		return;
    } 
    
    while (!in_file.fail()) {
        RatingInformation ratingInformation;
        in_file >> ratingInformation.userId;
        
        if (in_file.fail())
        {
            break;
        }
        
        in_file >> ratingInformation.movieId;
        in_file >> ratingInformation.rating;
        SimpleRecommender::ratings.push_back(ratingInformation);
    }
    
    in_file.close();
    return;
	
}

// prints the movies
void SimpleRecommender::printMovies() {
	
	int counter = 0;
	
	for (vector<MovieInformation>::iterator it = SimpleRecommender::movies.begin(); it != SimpleRecommender::movies.end(); ++it) {

		cout << (*it).getName() << endl;
		counter++;
		
	}

	if (counter == 0) {
	
		cout << "No recommendations found" << endl;
	
	}
	
}
