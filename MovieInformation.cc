// Rayyan Khoury
// 260257960
// COMP 322
// Assignment 3

#include "MovieInformation.h"

    //constructor takes as input a name and id and initializes the MovieInformation object accordingly
MovieInformation::MovieInformation(string moviename, int id) {

	name = moviename;
	movieId = id;

}

  //getName returns the name property
string MovieInformation::getName() const {

	return name;

}
  
  //getId returns the id property
int MovieInformation::getId() const {

	return movieId;

}
