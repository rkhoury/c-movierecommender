// Rayyan Khoury
// 260257960
// COMP 322
// Assignment 3

#include "MovieInformation.h"
#include "SimpleRecommender.h"
#include "RatingInformation.cc"

int main (int argc, char * const argv[]) {

	string ratings;
	string movies;
	
	cout << "Please enter a filename to load ratings from:" << endl;
	cin >> ratings;
	
	cout << "Please enter a filename to load movies from:" << endl;
	cin >> movies;
	
	SimpleRecommender recommend;
	
	recommend.loadMovieListFromFile(movies);
	recommend.loadRatingsFromFile(ratings);
	
	int quit;
	
	while (true) {
	
		cout << "Please type a user id:" << endl;
		cin >> quit;
	
		if (quit == -1) {
	
			break;
	
		}
		
		cout << "List of movies in order:" << endl;
		recommend.printMovies();

	}
	
    return 0;
	
}
